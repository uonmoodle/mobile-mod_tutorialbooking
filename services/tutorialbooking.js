// (C) Copyright 2017 University of Nottingham
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

angular.module('mm.addons.mod_tutorialbooking')

/**
 * Tutorial booking service.
 *
 * @module mm.addons.mod_tutorialbooking
 * @ngdoc controller
 * @name $mmaModTutorialbooking
 */
.factory('$mmaModTutorialbooking', function($mmSite, $q, $mmSitesManager, $mmUtil) {
    var self = {};

    /**
     * Get cache key for can sign up WS calls.
     *
     * @param  {Number} tutorialid Tutorial booking id.
     * @return {String} Cache key.
     */
    function capabilitiesCacheKey(tutorialid) {
        return 'mmaModTutorialbooking:capabilities:' + tutorialid;
    }

    /**
     * Get cache key for tutorial booking data WS calls.
     *
     * @param  {Number} tutorialid Tutorial booking id.
     * @return {String} Cache key.
     */
    function tutorialbookingDataCacheKey(tutorialid) {
        return 'mmamodTutorialbooking:tutorial:' + tutorialid;
    }

    /**
     * Gets the capabilities of the user on a Tutorial booking activity.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbooking#getCapabilities
     * @param {Number} tutorialid Tutorial booking id.
     * @returns {Promise} Resolves to an object containing the capabilities
     */
    self.getCapabilities = function(tutorialid) {
        var params = {
                id: tutorialid
            },
            preSets = {
                cacheKey: capabilitiesCacheKey(tutorialid)
            };

        return $mmSite.read('mod_tutorialbooking_capabilities', params, preSets).then(function(result) {
            if (result) {
                return result;
            }
            return $q.reject();
        });
    };

    /**
     * Refreshes the tutorial booking data.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbooking#refreshData
     * @param {Number} tutorialid Tutorial booking id.
     * @returns {Promise}
     */
    self.refreshData = function(tutorialid) {
        // Clear the data cache for the tutorial booking and then get the data.
        return $mmSite.invalidateWsCacheForKey(tutorialbookingDataCacheKey(tutorialid)).then(function() {
            return self.getData(tutorialid);
        });
    };

    /**
     * Gets the data for a Tutorial booking activity.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbooking#getData
     * @param {Number} tutorialid Tutorial booking id.
     * @returns {Promise}
     */
    self.getData = function(tutorialid) {
        var params = {
                id: tutorialid
            },
            preSets = {
                cacheKey: tutorialbookingDataCacheKey(tutorialid)
            };
        return $mmSite.read('mod_tutorialbooking_details', params, preSets).then(function(result) {
            if (result) {
                return result;
            }
            return $q.reject();
        });
    };

    /**
     * Signs the user up to a slot.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbooking#signup
     * @param {Number} slotid The id of a tutorial booking slot.
     * @returns {Promise}
     */
    self.signup = function(slotid) {
        var signupparams = {
                slotid: slotid
            };
        return $mmSite.write('mod_tutorialbooking_signup', signupparams).then(function(response) {
                if (response.success === true) {
                    return response.success;
                }
                return $q.reject({
                    error: response.error.message
                });
            });
    };

    /**
     * Removes the user's signup from the tutorial booking activity the slot is part of.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbooking#removeSignup
     * @param {Number} slotid The id of a tutorial booking slot.
     * @returns {Promise}
     */
    self.removeSignup = function(slotid) {
        var removeparams = {
                slotid: slotid
            };
        return $mmSite.write('mod_tutorialbooking_remove_signup', removeparams).then(function(response) {
                if (response.success === true) {
                    return response.success;
                }
                return $q.reject({
                    error: response.error.message
                });
            });
    };

    /**
     * Log that a user viewed the tutorial booking.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbooking#logView
     * @param {Number} tutorialid
     * @returns {Promise}
     */
    self.logView = function(tutorialid) {
        var viewparams = {
                id: tutorialid
            };
        return $mmSite.write('mod_tutorialbooking_view_tutorialbooking', viewparams);
    };

    /**
     * Return whether or not the plugin is enabled in a certain site. Plugin is enabled if the tutorial booking WS are available.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbooking#isPluginEnabled
     * @param  {String} siteId Site ID. If not defined, current site.
     * @return {Promise} Promise resolved with true if plugin is enabled, rejected or resolved with false otherwise.
     */
    self.isPluginEnabled = function(siteId) {
        siteId = siteId || $mmSite.getId();

        return $mmSitesManager.getSite(siteId).then(function(site) {
            return  site.wsAvailable('mod_tutorialbooking_view_tutorialbooking') &&
                    site.wsAvailable('mod_tutorialbooking_details') &&
                    site.wsAvailable('mod_tutorialbooking_signup') &&
                    site.wsAvailable('mod_tutorialbooking_remove_signup') &&
                    site.wsAvailable('mod_tutorialbooking_capabilities');
        });
    };

    return self;
});

