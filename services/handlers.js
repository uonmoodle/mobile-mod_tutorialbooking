// (C) Copyright 2017 University of Nottingham
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

angular.module('mm.addons.mod_tutorialbooking')

/**
 * Mod tutorialbooking handlers.
 *
 * @module mm.addons.mod_tutorialbooking
 * @ngdoc service
 * @name $mmaModTutorialbookingHandlers
 */
.factory('$mmaModTutorialbookingHandlers', function($state, $mmContentLinksHelper, $mmaModTutorialbooking) {
    var self = {};

    /**
     * Course content handler.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbookingHandlers#courseContent
     */
    self.courseContent = function() {
        var self = {};

        /**
         * Whether or not the module is enabled for the site.
         *
         * @return {Boolean}
         */
        self.isEnabled = function() {
            return $mmaModTutorialbooking.isPluginEnabled();
        };

        /**
         * Get the controller.
         *
         * @param {Object} module The module info.
         * @param {Number} The id of the course
         * @return {Function}
         */
        self.getController = function(module, courseId) {
            return function($scope) {
                $scope.title = module.name;
                $scope.icon = 'addons/mod/tutorialbooking/img/icon.svg';
                $scope.class = 'mma-mod_tutorialbooking-handler';
                $scope.spinner = false; // Show spinner while calculating status.

                $scope.action = function(e) {
                    if (e) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    $state.go('site.mod_tutorialbooking', {module: module, courseid: courseId});
                };
            };
        };

        return self;
    };

    /**
     * Content links handler for tutorialbooking index.
     *
     * @module mm.addons.mod_tutorialbooking
     * @ngdoc method
     * @name $mmaModTutorialbookingHandlers#indexLinksHandler
     */
    self.indexLinksHandler = $mmContentLinksHelper.createModuleIndexLinkHandler('mmaModTutorialbooking', 'tutorialbooking', $mmaModTutorialbooking);

    return self;
});

