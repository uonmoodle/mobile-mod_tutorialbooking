ABOUT
==========
The 'Tutorial Booking' plugin was developed by
    Neill Magill

This module may be distributed under the terms of the Apache License, Version 2.0
(see http://www.apache.org/licenses/LICENSE-2.0 for details)

PURPOSE
==========
The plugin was developed to be a remote Moodle Mobile plugin for the University of Nottingham's
Tutorial booking plugin (https://bitbucket.org/uonmoodle/moodle-mod_tutorialbooking)
