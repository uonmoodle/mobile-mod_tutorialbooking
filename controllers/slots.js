// (C) Copyright 2017 University of Nottingham
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

angular.module('mm.addons.mod_tutorialbooking')

/**
 * Tutorial booking slots controler
 *
 * @module mm.addons.mod_tutorialbooking
 * @ngdoc controller
 * @name mmaModTutorialbookingSlotsCtrl
 */
.controller('mmaModTutorialbookingSlotsCtrl', function($scope, $stateParams, $mmCourse, mmUserProfileState,
    mmaModTutorialbookingComponent, $mmaModTutorialbooking, $mmUtil, $mmText, $translate) {
    var module = $stateParams.module || {},
        courseid = $stateParams.courseid,
        tutorialbooking;

    // Store if the activity information has been loaded.
    $scope.tutorialbookingLoaded = false;
    $scope.capabilitiesLoaded = false;
    // Store some general information.
    $scope.moduleUrl = module.url;
    $scope.moduleName = $mmCourse.translateModuleName('tutorialbooking');
    $scope.courseid = courseid;
    $scope.instance = module.instance;
    $scope.userStateName = mmUserProfileState;
    $scope.component = mmaModTutorialbookingComponent;
    $scope.componentId = module.id;
    // Defaults for some things that will be set when the tutorial data is loaded.
    $scope.refreshIcon = 'spinner';
    $scope.title = module.name;
    $scope.description = module.description;
    $scope.canSignup = false;
    $scope.locked = false;
    $scope.privacy = 1;
    $scope.signedup = false;

    /**
     * Decides if a signup button should be displayed for a given slot.
     *
     * @param {Object} slot A slot from $scope.slots
     * @returns {Boolean}
     */
    $scope.displaySignup = function(slot) {
        return !$scope.signedup && !$scope.locked && $scope.freespaces(slot) > 0;
    };

    /**
     * Decides if a remove signup button should be displayed for a given slot.
     *
     * @param {Object} slot A slot from $scope.slots
     * @returns {Boolean}
     */
    $scope.displayRemoveSignup = function(slot) {
        return $scope.signedup && slot.signedup &&!$scope.locked;
    };

    /**
     * Detemines the css class that should be applied to the badge
     * detailing the number of free spaces in a slot.
     *
     * @param {Object} slot A slot from $scope.slots
     * @returns {String}
     */
    $scope.freeBadgeClass = function(slot) {
        var freespaces = slot.spaces - slot.usedspaces;
        var classname;
        if (freespaces < 1) {
            classname = 'badge-assertive';
        } else {
            classname = 'badge-balanced';
        }
        return classname;
    };

    /**
     * Given a slot calculate the number of free spaces.
     *
     * @param {Object} slot A slot from $scope.slots
     * @returns {Number}
     */
    $scope.freespaces = function(slot) {
        var freespaces = slot.spaces - slot.usedspaces;
        if (freespaces < 0) {
            freespaces = 0;
        }
        return freespaces;
    };

    // Assign the refresh data function to the scope.
    $scope.refresh = refreshData;

    /**
     * Context Menu Description action.
     *
     * @returns {undefined}
     */
    $scope.expandDescription = function() {
        $mmText.expandText($translate.instant('mm.core.description'), $scope.description, false,
                    mmaModTutorialbookingComponent, module.id);
    };

    /**
     * Removes a user's signup from the slot.
     *
     * @param {Number} id the id of a tutorial slot.
     * @returns {Promise}
     */
    $scope.removeSignup = function (id) {
        return $mmaModTutorialbooking.removeSignup(id).then(refreshData).catch(function(message) {
            $mmUtil.showErrorModalDefault(message, 'mma.mod_tutorialbooking.cannotremove', true);
            return refreshData();
        });
    };

    /**
     * Signs the user up to a slot.
     *
     * @param {Number} id The id of a tutorial slot
     * @returns {Promise}
     */
    $scope.signup = function (id) {
        return $mmaModTutorialbooking.signup(id).then(refreshData).catch(function(message) {
            $mmUtil.showErrorModalDefault(message, 'mma.mod_tutorialbooking.cannotsignup', true);
            return refreshData();
        });
    };

    // Get the user's capabilities for this tutorialbooking activity.
    $mmaModTutorialbooking.getCapabilities(module.instance).then(processCapabilities);
    loadData();

    /**
     * Loads data for the tutorial booking activity.
     *
     * @returns {Promise}
     */
    function loadData() {
        // Get the Tutorialbooking data.
        return $mmaModTutorialbooking.getData($scope.instance).then(processData).catch(function(message) {
            $mmUtil.showErrorModalDefault(message, 'mma.mod_tutorialbooking.cannotload', true);
        }).finally(logview);
   }

   /**
    * Log that the user viewed the tutorial booking activity.
    *
    * @returns {undefined}
    */
   function logview() {
        $mmaModTutorialbooking.logView(module.instance).then(function() {
            $mmCourse.checkModuleCompletion(courseid, module.completionstatus);
        });
   }

    /**
     * Refreshes the tutorial booking activity.
     *
     * @returns {Promise}
     */
   function refreshData() {
        // When this function is called we should set the data to be not loaded.
        $scope.tutorialbookingLoaded = false;
        $scope.refreshIcon = 'spinner';
        // Get the Tutorialbooking data.
        return $mmaModTutorialbooking.refreshData($scope.instance).then(processData).catch(function(message) {
            $mmUtil.showErrorModalDefault(message, 'mma.mod_tutorialbooking.cannotload', true);
        }).finally(refreshCompleted);
   }

   /**
    * Actions to be performed after a refresh has been completed.
    *
    * @returns {undefined}
    */
   function refreshCompleted() {
       $scope.$broadcast('scroll.refreshComplete');
   }

    /**
     * Process the data returned by $mmaModTutorialbooking.getData, used when loading and refreshing.
     *
     * @param {Object} tutorialbooking The value returned by the $mmaModTutorialbooking.getData promise.
     * @returns {undefined}
     */
    function processData(tutorialbooking) {
        $scope.title = tutorialbooking.title || $scope.title;
        $scope.description = tutorialbooking.intro || $scope.description;
        $scope.privacy = tutorialbooking.privacy;
        $scope.signedup = tutorialbooking.signedup;
        $scope.locked = tutorialbooking.locked;
        $scope.slots = tutorialbooking.slots || [];

        // Set to a loaded state.
        $scope.refreshIcon = 'ion-refresh';
        $scope.tutorialbookingLoaded = true;
    }

    /**
     * Gets the capabilities for the tutorial booking and determines which actions the user can perform.
     *
     * @param {Object} capabilities
     * @returns {undefined}
     */
    function processCapabilities(capabilities) {
        $scope.canSignup = capabilities.submit;
        // The user's capabilities have been loaded.
        $scope.capabilitiesLoaded = true;
    }
});
