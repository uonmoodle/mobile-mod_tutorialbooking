// (C) Copyright 2017 university of Nottingham
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

angular.module('mm.addons.mod_tutorialbooking', ['mm.core'])
.constant('mmaModTutorialbookingComponent', 'mmaModTutorialbooking')

.config(function($stateProvider) {
    $stateProvider
    .state('site.mod_tutorialbooking', {
        url: '/mod_tutorialbooking',
        params: {
            module: null,
            courseid: null
        },
        views: {
            'site': {
                controller: 'mmaModTutorialbookingSlotsCtrl',
                templateUrl: 'addons/mod/tutorialbooking/templates/slots.html'
            }
        }
    });
})
.config(function($mmCourseDelegateProvider, $mmContentLinksDelegateProvider) {
    $mmCourseDelegateProvider.registerContentHandler('mmaModTutorialbooking', 'tutorialbooking', '$mmaModTutorialbookingHandlers.courseContent');
    $mmContentLinksDelegateProvider.registerLinkHandler('mmaModTutorialbooking:index', '$mmaModTutorialbookingHandlers.indexLinksHandler');
});
